from datetime import date
import json

# function to load Json file data
def loadJsonFileData():
	f = open('data.json',)
	data = json.load(f)
	f.close()
	return data

# function to print data
def printData(data):
	for i in data:
		print(i)
		for j in data[i]:
			print(j)

# function to update profile of an employee based on from and to dates
def updateEmpData(dict):
	name = input('Employe name to update: ')
	if name in dict.keys():
		fromDate = input('current from date: ')
		toDate = input('current to date: ')
		check = 0
		for i in dict[name]:
			if(fromDate == i['from']) and (toDate == i['to']):
				i['profile'] = input('Enter profile to update: ')
				print(i)
				check = 1
				break
		if(check == 0):
			print('Wrong dates: ')
	else:
		print('No record found! ')

# function to search profile based on from and to dates
def searchProfile(data):
	name = input('Enter employee name: ')
	if name in data.keys():
		d, m, y = [int(x) for x in input('Enter date (DD-MM-YYYY):').split('-')]
		inputDate = date(y, m, d)
		check=0
		for i in data[name]:
			d2, m2, y2 = [int (x) for x in i['from'].split('-')]
			fromDate = date(y2, m2, d2)
			d3, m3, y3 = [int (x) for x in i['to'].split('-')]
			toDate = date(y3, m3, d3)
			if fromDate < inputDate < toDate:
				print(i['profile'])
				check =1
				break
		if(check!=1):
			print('No records found for given date')
	else:
		print('No record found! ')
	


print('Loading Json data from data.json file...')
data = loadJsonFileData()

print('Printing data...')
printData(data)

print('To update employee profile based on from and to dates...')
updateEmpData(data)

print('To search profile of employee on specific day...')
searchProfile(data)


